provider "aws" {
  region = "us-west-2"
}

data "aws_route53_zone" "hashidemos-io" {
  name         = "hashidemos.io."
  private_zone = false
}

resource "aws_route53_record" "nathan-test-lb" {
  zone_id = "${data.aws_route53_zone.hashidemos-io.zone_id}"
  name    = "nathan-test-lb.${data.aws_route53_zone.hashidemos-io.name}"
  type    = "A"

  alias {
    name                   = "${aws_elb.nathan-test-lb.dns_name}"
    zone_id                = "${aws_elb.nathan-test-lb.zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_elb" "nathan-test-lb" {
  name               = "nathan-test-lb"
  availability_zones = ["us-west-2a"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}
